-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.0.17-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table oauth2.authorities
CREATE TABLE IF NOT EXISTS `authorities` (
  `username` varchar(255) NOT NULL,
  `authority` varchar(50) NOT NULL,
  UNIQUE KEY `ix_auth_username` (`username`,`authority`),
  CONSTRAINT `fk_authorities_users` FOREIGN KEY (`username`) REFERENCES `users` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table oauth2.authorities: ~9 rows (approximately)
/*!40000 ALTER TABLE `authorities` DISABLE KEYS */;
INSERT INTO `authorities` (`username`, `authority`) VALUES
	('user_01', 'ROLE_CLIENT'),
	('user_01', 'ROLE_USER'),
	('user_02', 'ROLE_ADMIN'),
	('user_02', 'ROLE_CLIENT'),
	('user_02', 'ROLE_TRUSTED_CLIENT'),
	('user_02', 'ROLE_USER');
/*!40000 ALTER TABLE `authorities` ENABLE KEYS */;


-- Dumping structure for table oauth2.clientdetails
CREATE TABLE IF NOT EXISTS `clientdetails` (
  `appId` varchar(256) NOT NULL,
  `resourceIds` varchar(256) DEFAULT NULL,
  `appSecret` varchar(256) DEFAULT NULL,
  `scope` varchar(256) DEFAULT NULL,
  `grantTypes` varchar(256) DEFAULT NULL,
  `redirectUrl` varchar(256) DEFAULT NULL,
  `authorities` varchar(256) DEFAULT NULL,
  `access_token_validity` int(11) DEFAULT NULL,
  `refresh_token_validity` int(11) DEFAULT NULL,
  `additionalInformation` varchar(4096) DEFAULT NULL,
  `autoApproveScopes` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`appId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table oauth2.clientdetails: ~0 rows (approximately)
/*!40000 ALTER TABLE `clientdetails` DISABLE KEYS */;
/*!40000 ALTER TABLE `clientdetails` ENABLE KEYS */;


-- Dumping structure for table oauth2.oauth_access_token
CREATE TABLE IF NOT EXISTS `oauth_access_token` (
  `token_id` varchar(256) DEFAULT NULL,
  `token` longtext,
  `authentication_id` varchar(256) NOT NULL,
  `user_name` varchar(256) DEFAULT NULL,
  `client_id` varchar(256) DEFAULT NULL,
  `authentication` longtext,
  `refresh_token` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`authentication_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping structure for table oauth2.oauth_approvals
CREATE TABLE IF NOT EXISTS `oauth_approvals` (
  `userId` varchar(256) DEFAULT NULL,
  `clientId` varchar(256) DEFAULT NULL,
  `scope` varchar(256) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  `expiresAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `lastModifiedAt` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table oauth2.oauth_approvals: ~0 rows (approximately)
/*!40000 ALTER TABLE `oauth_approvals` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_approvals` ENABLE KEYS */;


-- Dumping structure for table oauth2.oauth_client_details
CREATE TABLE IF NOT EXISTS `oauth_client_details` (
  `client_id` varchar(256) NOT NULL,
  `resource_ids` varchar(256) DEFAULT NULL,
  `client_secret` varchar(256) DEFAULT NULL,
  `scope` varchar(256) DEFAULT NULL,
  `authorized_grant_types` varchar(256) DEFAULT NULL,
  `web_server_redirect_uri` varchar(256) DEFAULT NULL,
  `authorities` varchar(256) DEFAULT NULL,
  `access_token_validity` int(11) DEFAULT NULL,
  `refresh_token_validity` int(11) DEFAULT NULL,
  `additional_information` varchar(4096) DEFAULT NULL,
  `autoapprove` varchar(256) DEFAULT NULL,
  `name` varchar(256) DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table oauth2.oauth_client_details: ~12 rows (approximately)
/*!40000 ALTER TABLE `oauth_client_details` DISABLE KEYS */;
INSERT INTO `oauth_client_details` (`client_id`, `resource_ids`, `client_secret`, `scope`, `authorized_grant_types`, `web_server_redirect_uri`, `authorities`, `access_token_validity`, `refresh_token_validity`, `additional_information`, `autoapprove`, `name`, `description`) VALUES
	('client1', NULL, 'asdfasdf', 'read,write,delete', 'password,authorization_code,refresh_token,implicit', NULL, 'ROLE_CLIENT, ROLE_TRUSTED_CLIENT', NULL, NULL, NULL, '0', 'Client 1', NULL),
	('client10', NULL, 'asdfasdf', 'read,write,delete', 'password,refresh_token,implicit', NULL, 'ROLE_CLIENT, ROLE_TRUSTED_CLIENT', NULL, NULL, NULL, '0', 'zx', 'qwe'),
	('client11', NULL, 'asdfasdf', 'read,write,delete', 'password,authorization_code,refresh_token,implicit', NULL, 'ROLE_CLIENT, ROLE_TRUSTED_CLIENT', NULL, NULL, NULL, '0', NULL, NULL),
	('client2', NULL, 'qweqrqr', 'read,write,delete', 'password,authorization_code,refresh_token,implicit', NULL, 'ROLE_CLIENT, ROLE_TRUSTED_CLIENT', NULL, NULL, NULL, '1', NULL, NULL),
	('client3', NULL, 'asdfasdf', 'read,write,delete', 'password,authorization_code,refresh_token,implicit', NULL, 'ROLE_CLIENT, ROLE_TRUSTED_CLIENT', NULL, NULL, NULL, '0', NULL, NULL),
	('client4', NULL, 'asdfasdf', 'read,write,delete', 'password,authorization_code,refresh_token,implicit', NULL, 'ROLE_CLIENT, ROLE_TRUSTED_CLIENT', NULL, NULL, NULL, '0', NULL, NULL),
	('client6', NULL, 'asdfasdf', 'read,write,delete', 'password,authorization_code,refresh_token,implicit', NULL, 'ROLE_CLIENT, ROLE_TRUSTED_CLIENT', NULL, NULL, NULL, '0', NULL, NULL),
	('client7', NULL, 'asdfasdf', 'read,write,delete', 'password,authorization_code,refresh_token,implicit', NULL, 'ROLE_CLIENT, ROLE_TRUSTED_CLIENT', NULL, NULL, NULL, '0', NULL, NULL),
	('client8', NULL, 'asdfasdf', 'read,write,delete', 'password,authorization_code,refresh_token,implicit', NULL, 'ROLE_CLIENT, ROLE_TRUSTED_CLIENT', NULL, NULL, NULL, '0', NULL, NULL),
	('client9', NULL, 'asdfasdf', 'read,write,delete', 'password,authorization_code,refresh_token,implicit', NULL, 'ROLE_CLIENT, ROLE_TRUSTED_CLIENT', NULL, NULL, NULL, '0', NULL, NULL);
/*!40000 ALTER TABLE `oauth_client_details` ENABLE KEYS */;


-- Dumping structure for table oauth2.oauth_client_token
CREATE TABLE IF NOT EXISTS `oauth_client_token` (
  `token_id` varchar(256) DEFAULT NULL,
  `token` longtext,
  `authentication_id` varchar(256) NOT NULL,
  `user_name` varchar(256) DEFAULT NULL,
  `client_id` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`authentication_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table oauth2.oauth_client_token: ~0 rows (approximately)
/*!40000 ALTER TABLE `oauth_client_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_client_token` ENABLE KEYS */;


-- Dumping structure for table oauth2.oauth_code
CREATE TABLE IF NOT EXISTS `oauth_code` (
  `code` varchar(256) DEFAULT NULL,
  `authentication` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table oauth2.oauth_code: ~0 rows (approximately)
/*!40000 ALTER TABLE `oauth_code` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_code` ENABLE KEYS */;


-- Dumping structure for table oauth2.oauth_refresh_token
CREATE TABLE IF NOT EXISTS `oauth_refresh_token` (
  `token_id` varchar(256) DEFAULT NULL,
  `token` longtext,
  `authentication` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping structure for table oauth2.person
CREATE TABLE IF NOT EXISTS `person` (
  `ID` int(11) NOT NULL,
  `FIRST_NAME` varchar(100) DEFAULT NULL,
  `LAST_NAME` varchar(100) DEFAULT NULL,
  `NUMBER` int(11) DEFAULT NULL,
  `BRITH_DATE` date DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table oauth2.person: ~12 rows (approximately)
/*!40000 ALTER TABLE `person` DISABLE KEYS */;
INSERT INTO `person` (`ID`, `FIRST_NAME`, `LAST_NAME`, `NUMBER`, `BRITH_DATE`) VALUES
	(1, 'firstName', 'lastName', 3, '2008-08-22'),
	(2, 'firstName', 'lastName', 3, '2008-08-22'),
	(3, 'firstName', 'lastName', 3, '2008-08-22'),
	(4, 'firstName', 'lastName', 3, '2008-08-22'),
	(5, 'firstName', 'lastName', 3, '2008-08-22'),
	(6, 'firstName', 'lastName', 3, '2008-08-22'),
	(7, 'firstName', 'lastName', 3, '2008-08-22'),
	(8, 'firstName', 'lastName', 3, '2008-08-22'),
	(9, 'firstName', 'lastName', 3, '2008-08-22'),
	(10, 'firstName', 'lastName', 3, '2008-08-22'),
	(11, 'firstName', 'lastName', 3, '2008-08-22'),
	(12, 'firstName', 'lastName', 3, '2008-08-22');
/*!40000 ALTER TABLE `person` ENABLE KEYS */;


-- Dumping structure for table oauth2.users
CREATE TABLE IF NOT EXISTS `users` (
  `username` varchar(255) NOT NULL,
  `password` varchar(50) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table oauth2.users: ~4 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`username`, `password`, `enabled`) VALUES
	('user_01', '5f4dcc3b5aa765d61d8327deb882cf99', 1),
	('user_02', '5f4dcc3b5aa765d61d8327deb882cf99', 1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
